package it.de.microtema.process.domain.rest;

import de.microtema.process.domain.Application;
import de.microtema.process.domain.rest.ProcessController;
import de.microtema.process.domain.template.vo.TemplateProperties;
import de.microtema.process.domain.vo.DomainData;
import de.microtema.process.domain.vo.ResponseData;
import de.microtema.process.domain.vo.ResponseDataStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.inject.Inject;
import java.util.Collections;

import static de.microtema.model.builder.ModelBuilderFactory.min;
import static org.junit.jupiter.api.Assertions.*;

@EnableKafka
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:3333", "port=3333"})
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProcessControllerIT {

    @Inject
    ProcessController sut;

    @Inject
    TestRestTemplate restTemplate;

    DomainData domainData = min(DomainData.class);

    String url = "/rest/process";

    ParameterizedTypeReference<ResponseData<String>> typeReference = new ParameterizedTypeReference<ResponseData<String>>() {
    };

    @BeforeEach
    public void setup() {
        restTemplate.getRestTemplate().setInterceptors(Collections.singletonList(((request, body, execution) -> {

            String token = "Bearer eyJraWQiOiJoa1FJYU5qMGM3M3k2ekQ5M3Z6eTluM2k4czRqb21pQmViWDI0d3dGNlNvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULm9Ia0pNUDZFRHo5ejNiZGRjWU5OWVA1RzV2ZDBCeGU0NGJuVGFrY29Fc1kiLCJpc3MiOiJodHRwczovL2Rldi0xMDY1NzgyLm9rdGEuY29tL29hdXRoMi9kZWZhdWx0IiwiYXVkIjoiYXBpOi8vZGVmYXVsdCIsImlhdCI6MTYxNDI3OTQyNiwiZXhwIjoxNjE0MjgzMDI2LCJjaWQiOiIwb2EzaDBtcTJrd1J4ZTloTzVkNiIsInVpZCI6IjAwdTNoMjFjbTFDdXpoQVhWNWQ2Iiwic2NwIjpbIm9wZW5pZCIsInByb2ZpbGUiLCJlbWFpbCJdLCJzdWIiOiJtaWNyb3RlbWFAd2ViLmRlIn0.CY0-wR1DDeUrbp8JoS7qmTV_7tVgdrCbit1ka-BiMCzMvsglM-GsFQll_8OfODMSKmAorvzJIgmNzVXqHMYnkyJU5BNoqTnkyh-s7LAPxD4Vc0XH8B0w8VAc7jXbu4u8fTy72L1ZpXow8zK_-dtXCkSdjCEmopUOsDNkZB9j0uQf8kA5flsgy_B-JyULvHq9VhI-pOaOhUO67T9fXI0ezMgydhk0p-mz_F_g2uhMKEZRIfdNLyw_6DRXuGvx6uYLhj7C1ZZOq_9Bf0EP6g9ywpG510ATPSa2_pYazxk-d0QPuCcLCdb4LeQEfDEOC4QQEJczhGfjRPWdTOfXLfg8OA";

            HttpHeaders headers = request.getHeaders();
            headers.add("Authorization", token);

            return execution.execute(request, body);
        })));
    }

    @Test
    void startProcess() {

        HttpEntity<DomainData> requestEntity = new HttpEntity<>(domainData);

        ResponseEntity<ResponseData<String>> response = restTemplate.exchange(url + "/start", HttpMethod.POST, requestEntity, typeReference);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(ResponseDataStatus.OK, response.getBody().getStatus());
        assertNotNull(response.getBody().getValue());
    }

    @Test
    void startProcessWillThrowException() {

        // Given Invalid Data
        domainData.setReferenceId(null);
        HttpEntity<DomainData> requestEntity = new HttpEntity<>(domainData);

        // When
        ResponseEntity<ResponseData<String>> response = restTemplate.exchange(url + "/start", HttpMethod.POST, requestEntity, typeReference);

        // Then
        assertNotNull(response);
        assertSame(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(ResponseDataStatus.ERROR, response.getBody().getStatus());
        assertNotNull(response.getBody().getMessage());
        assertNull(response.getBody().getValue());
    }

    @Test
    void getTemplateProperties() {

        ResponseEntity<TemplateProperties> response = restTemplate.exchange(url + "/template", HttpMethod.GET, null, TemplateProperties.class);

        assertNotNull(response);
        assertSame(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }
}