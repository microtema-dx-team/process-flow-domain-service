package de.microtema.process.domain.converter;

import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.process.domain.vo.CurrentUser;
import de.microtema.process.domain.vo.DomainData;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DomainDataToProcessInstanceStartEventConverterTest {

    @Inject
    DomainDataToProcessInstanceStartEventConverter sut;

    @Model
    DomainData domainData;

    @Model
    CurrentUser currentUser;

    @BeforeEach
    void setUp() {
        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        ProcessInstanceStartEvent answer = sut.convert(domainData, currentUser);

        assertNotNull(answer);

        assertNotNull(answer.getBusinessKey());
        assertNotNull(answer.getStartTime());

        assertEquals(domainData.getReferenceId(), answer.getReferenceId());
        assertEquals(domainData.getReferenceType().toString(), answer.getReferenceType());
        assertEquals(domainData.getReferenceValue(), answer.getReferenceValue());

        assertEquals(domainData.getDefinitionKey(), answer.getDefinitionKey());

        assertEquals(currentUser.getUsername(), answer.getStarterId());
    }
}
