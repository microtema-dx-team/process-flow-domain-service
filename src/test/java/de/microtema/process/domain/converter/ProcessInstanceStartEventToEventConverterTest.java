package de.microtema.process.domain.converter;

import de.microtema.model.builder.annotation.Inject;
import de.microtema.model.builder.annotation.Model;
import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProcessInstanceStartEventToEventConverterTest {

    @Inject
    ProcessInstanceStartEventToEventConverter sut;

    @Model
    ProcessInstanceStartEvent model;

    @BeforeEach
    void setUp() {

       FieldInjectionUtil.injectFields(this);
    }

    @Test
    void convert() {

        String answer = sut.convert(model);

        assertNotNull(answer);
    }
}