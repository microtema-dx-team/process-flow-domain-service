package de.microtema.process.domain.template.service;

import de.microtema.model.builder.util.FieldInjectionUtil;
import de.microtema.process.domain.template.enums.ReferenceType;
import de.microtema.process.domain.template.enums.TemplatePropertyType;
import de.microtema.process.domain.template.vo.PropertyConstraint;
import de.microtema.process.domain.template.vo.TemplateProperties;
import de.microtema.process.domain.template.vo.TemplateProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TemplatePropertiesServiceTest {

    @Inject
    TemplatePropertiesService sut;

    @BeforeEach
    void setUp() {

        FieldInjectionUtil.injectFields(this);
    }

    @Test
    void getTemplateProperties() {

        TemplateProperties answer = sut.getTemplateProperties();

        assertNotNull(answer);
        assertEquals("DomainData", answer.getName());
        assertEquals("de.microtema.process.domain.vo.DomainData", answer.getId());

        List<TemplateProperty> properties = answer.getProperties();
        assertEquals(3, properties.size());
        int index = 0;
        TemplateProperty templateProperty = properties.get(index++);
        assertEquals(TemplatePropertyType.STRING, templateProperty.getType());
        assertEquals("de.microtema.process.domain.vo.DomainData.referenceId", templateProperty.getId());
        assertEquals("referenceId", templateProperty.getName());
        assertEquals("Domain Data", templateProperty.getGroupLabel());
        assertEquals("Reference Id", templateProperty.getLabel());
        assertTrue(templateProperty.isEditable());
        assertNull(templateProperty.getValue());
        assertTrue(templateProperty.getChoices().isEmpty());
        PropertyConstraint constraints = templateProperty.getConstraints();
        assertTrue(constraints.isNotNull());


        templateProperty = properties.get(index++);
        assertEquals(TemplatePropertyType.DROPDOWN, templateProperty.getType());
        assertEquals("de.microtema.process.domain.vo.DomainData.referenceType", templateProperty.getId());
        assertEquals("referenceType", templateProperty.getName());
        assertEquals("Domain Data", templateProperty.getGroupLabel());
        assertEquals("Reference Type", templateProperty.getLabel());
        assertTrue(templateProperty.isEditable());
        assertEquals(ReferenceType.DOMAIN, templateProperty.getValue());
        assertFalse(templateProperty.getChoices().isEmpty());
        constraints = templateProperty.getConstraints();
        assertTrue(constraints.isNotNull());

        templateProperty = properties.get(index);
        assertEquals(TemplatePropertyType.STRING, templateProperty.getType());
        assertEquals("de.microtema.process.domain.vo.DomainData.referenceValue", templateProperty.getId());
        assertEquals("referenceValue", templateProperty.getName());
        assertEquals("Domain Data", templateProperty.getGroupLabel());
        assertEquals("Reference Value", templateProperty.getLabel());
        assertTrue(templateProperty.isEditable());
        assertNull(templateProperty.getValue());
        assertTrue(templateProperty.getChoices().isEmpty());
        constraints = templateProperty.getConstraints();
        assertTrue(constraints.isNotNull());
    }
}
