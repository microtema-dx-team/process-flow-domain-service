package de.microtema.process.domain.kafka;

import de.microtema.process.domain.converter.ProcessInstanceStartEventToEventConverter;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class StartProcessProducer {

    private final ProcessInstanceStartEventToEventConverter eventConverter;

    private final StartProcessProperties startProcessProperties;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendEvent(ProcessInstanceStartEvent startEvent) {

        String topicName = startProcessProperties.getTopicName();

        String event = eventConverter.convert(startEvent);

        log.info(() -> "fire event: " + topicName);

        kafkaTemplate.send(topicName, event);
    }
}
