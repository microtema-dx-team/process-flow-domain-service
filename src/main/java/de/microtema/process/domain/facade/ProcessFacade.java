package de.microtema.process.domain.facade;

import de.microtema.process.domain.service.StartProcessService;
import de.microtema.process.domain.template.service.TemplatePropertiesService;
import de.microtema.process.domain.template.vo.TemplateProperties;
import de.microtema.process.domain.vo.DomainData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProcessFacade {

    private final StartProcessService startProcessService;

    private final TemplatePropertiesService templatePropertiesService;

    public String startProcess(DomainData domainData) {

        return startProcessService.startProcess(domainData);
    }

    public TemplateProperties getTemplateProperties() {

        return templatePropertiesService.getTemplateProperties();
    }
}
