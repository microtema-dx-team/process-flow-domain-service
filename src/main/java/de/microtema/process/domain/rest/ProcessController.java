package de.microtema.process.domain.rest;

import de.microtema.process.domain.facade.ProcessFacade;
import de.microtema.process.domain.template.vo.TemplateProperties;
import de.microtema.process.domain.vo.DomainData;
import de.microtema.process.domain.vo.ResponseData;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/rest/process")
public class ProcessController {

    private final ProcessFacade facade;

    @PostMapping(value = "/start")
    public ResponseEntity<ResponseData<String>> startProcess(@RequestBody @Valid DomainData domainData) {

        return ResponseEntity.ok(ResponseData.ok(facade.startProcess(domainData)));
    }

    @GetMapping(value = "/template")
    public ResponseEntity<TemplateProperties> getTemplateProperties() {

        return ResponseEntity.ok(facade.getTemplateProperties());
    }
}
