package de.microtema.process.domain.service;

import de.microtema.process.domain.converter.DomainDataToProcessInstanceStartEventConverter;
import de.microtema.process.domain.kafka.StartProcessProducer;
import de.microtema.process.domain.vo.CurrentUser;
import de.microtema.process.domain.vo.DomainData;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StartProcessService {

    private final CurrentUserService currentUserService;

    private final StartProcessProducer startProcessProducer;

    private final DomainDataToProcessInstanceStartEventConverter eventConverter;

    public String startProcess(DomainData domainData) {

        CurrentUser currentUser = currentUserService.getCurrentUser();

        ProcessInstanceStartEvent instanceStartEvent = eventConverter.convert(domainData, currentUser);

        startProcessProducer.sendEvent(instanceStartEvent);

        return instanceStartEvent.getBusinessKey();
    }
}
