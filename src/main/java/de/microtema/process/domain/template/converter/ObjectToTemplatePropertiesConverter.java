package de.microtema.process.domain.template.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.domain.template.annotation.TemplateProperty;
import de.microtema.process.domain.template.vo.TemplateProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class ObjectToTemplatePropertiesConverter implements Converter<TemplateProperties, Object> {

    private final FieldToTemplatePropertyConverter templatePropertyConverter;

    @Override
    public void update(TemplateProperties dest, Object orig) {

        Class<?> type = orig.getClass();

        dest.setId(type.getName());
        dest.setName(type.getSimpleName());

        List<Field> sortedFields = sortedFields(type);
        dest.setProperties(templatePropertyConverter.convertList(sortedFields, orig));
    }

    private List<Field> sortedFields(Class<?> type) {

        Field[] declaredFields = type.getDeclaredFields();

        return Stream.of(declaredFields).filter(it -> it.isAnnotationPresent(TemplateProperty.class)).sorted((o1, o2) -> {

            TemplateProperty annotation = o1.getAnnotation(TemplateProperty.class);
            TemplateProperty other = o2.getAnnotation(TemplateProperty.class);

            return annotation.order() - other.order();
        }).collect(Collectors.toList());
    }
}
