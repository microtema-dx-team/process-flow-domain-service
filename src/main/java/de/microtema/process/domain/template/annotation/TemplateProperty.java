package de.microtema.process.domain.template.annotation;

import de.microtema.process.domain.template.enums.TemplatePropertyType;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TemplateProperty {

    String groupLabel() default "#default";

    String label() default "#default";

    String description() default "";

    int order();

    TemplatePropertyType type() default TemplatePropertyType.UNDEFINED;

    String serviceUrl() default "";

    boolean editable() default true;

    Choice[] choices() default {};

    Constraint constraints();
}
