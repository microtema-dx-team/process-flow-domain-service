package de.microtema.process.domain.template.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Data
public class TemplateProperties {

    @NotNull
    private String id;

    @NotNull
    private String name;

    private List<TemplateProperty> properties = Collections.emptyList();
}
