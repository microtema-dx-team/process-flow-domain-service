package de.microtema.process.domain.template.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Constraint {

    /**
     * Input must be non-empty
     *
     * @return boolean
     */
    boolean notNull() default false;

    /**
     * Minimal length for the input
     *
     * @return int
     */
    int minLength() default -1;

    /**
     * Maximal length for the input
     *
     * @return int
     */
    int maxLength() default -1;

    /**
     * Regular expression to match the input against
     */
    String pattern() default "";
}
