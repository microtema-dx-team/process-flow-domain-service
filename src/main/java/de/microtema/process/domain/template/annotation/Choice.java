package de.microtema.process.domain.template.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Choice {

    String name();

    String value();
}
