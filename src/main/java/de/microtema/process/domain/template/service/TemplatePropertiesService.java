package de.microtema.process.domain.template.service;

import de.microtema.process.domain.template.converter.ObjectToTemplatePropertiesConverter;
import de.microtema.process.domain.template.vo.TemplateProperties;
import de.microtema.process.domain.vo.DomainData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TemplatePropertiesService {

    private final ObjectToTemplatePropertiesConverter propertiesConverter;

    public TemplateProperties getTemplateProperties() {

        return propertiesConverter.convert(new DomainData());
    }
}
