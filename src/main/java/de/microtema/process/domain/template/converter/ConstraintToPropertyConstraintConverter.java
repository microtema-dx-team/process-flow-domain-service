package de.microtema.process.domain.template.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.domain.template.annotation.Constraint;
import de.microtema.process.domain.template.vo.PropertyConstraint;
import org.springframework.stereotype.Component;

@Component
public class ConstraintToPropertyConstraintConverter implements Converter<PropertyConstraint, Constraint> {

    @Override
    public void update(PropertyConstraint dest, Constraint orig) {

        dest.setNotNull(orig.notNull());
        dest.setMinLength(orig.minLength());
        dest.setMaxLength(orig.maxLength());
        dest.setPattern(orig.pattern());
    }
}
