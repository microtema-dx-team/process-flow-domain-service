package de.microtema.process.domain.template.vo;

import de.microtema.process.domain.template.enums.TemplatePropertyType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Data
public class TemplateProperty {

    @NotNull
    private String id;

    @NotNull
    private String name;

    private Object value;

    private String description;

    @NotNull
    private String label;

    @NotNull
    private String groupLabel;

    @NotNull
    private TemplatePropertyType type;

    @NotNull
    private String serviceUrl;

    private boolean editable;

    private PropertyConstraint constraints;

    private List<PropertyChoice> choices = Collections.emptyList();
}
