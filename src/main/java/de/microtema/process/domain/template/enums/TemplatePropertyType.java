package de.microtema.process.domain.template.enums;

public enum TemplatePropertyType {

    STRING, BOOLEAN, DROPDOWN, TEXT, HIDDEN, NUMBER, UNDEFINED
}
