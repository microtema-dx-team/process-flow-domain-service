package de.microtema.process.domain.template.enums;

public enum ReferenceType {
    DOMAIN, DATA
}
