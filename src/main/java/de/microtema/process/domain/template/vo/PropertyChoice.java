package de.microtema.process.domain.template.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PropertyChoice {

    @NotNull
    private String name;

    private Object value;
}
