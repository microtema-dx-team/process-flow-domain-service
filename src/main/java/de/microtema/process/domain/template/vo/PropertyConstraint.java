package de.microtema.process.domain.template.vo;

import lombok.Data;

@Data
public class PropertyConstraint {

    /**
     * Input must be non-empty
     */
    private boolean notNull;

    /**
     * Minimal length for the input
     */
    private int minLength;

    /**
     * Maximal length for the input
     */
    private int maxLength;

    /**
     * Regular expression to match the input against
     */
    private String pattern;
}
