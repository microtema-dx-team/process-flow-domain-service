package de.microtema.process.domain.template.converter;

import de.microtema.model.converter.Converter;
import de.microtema.process.domain.template.annotation.Choice;
import de.microtema.process.domain.template.vo.PropertyChoice;
import org.springframework.stereotype.Component;

@Component
public class ChoiceToPropertyChoiceConverter implements Converter<PropertyChoice, Choice> {

    @Override
    public void update(PropertyChoice dest, Choice orig) {

        dest.setName(orig.name());
        dest.setValue(orig.value());
    }
}
