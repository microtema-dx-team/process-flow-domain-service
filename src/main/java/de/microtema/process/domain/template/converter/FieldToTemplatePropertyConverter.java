package de.microtema.process.domain.template.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.process.domain.template.annotation.TemplateProperty;
import de.microtema.process.domain.template.enums.TemplatePropertyType;
import de.microtema.process.domain.template.vo.PropertyChoice;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class FieldToTemplatePropertyConverter implements MetaConverter<de.microtema.process.domain.template.vo.TemplateProperty, Field, Object> {

    private static final Map<Class<?>, TemplatePropertyType> TYPE_MAPPING = new HashMap<>();

    static {
        TYPE_MAPPING.put(String.class, TemplatePropertyType.STRING);
        TYPE_MAPPING.put(Character.class, TemplatePropertyType.STRING);

        TYPE_MAPPING.put(Integer.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(int.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(Long.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(long.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(Double.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(double.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(Float.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(float.class, TemplatePropertyType.NUMBER);
        TYPE_MAPPING.put(BigInteger.class, TemplatePropertyType.NUMBER);

        TYPE_MAPPING.put(Boolean.class, TemplatePropertyType.BOOLEAN);
        TYPE_MAPPING.put(boolean.class, TemplatePropertyType.BOOLEAN);
    }

    private final ChoiceToPropertyChoiceConverter propertyChoiceConverter;

    private final ConstraintToPropertyConstraintConverter propertyConstraintConverter;

    @Override
    public void update(de.microtema.process.domain.template.vo.TemplateProperty dest, Field orig, Object meta) {

        dest.setId(meta.getClass().getName() + "." + orig.getName());
        dest.setName(orig.getName());
        dest.setValue(getValue(orig, meta));

        TemplateProperty annotation = orig.getAnnotation(TemplateProperty.class);

        dest.setGroupLabel(getLabelOrDefault(annotation.groupLabel(), meta.getClass().getSimpleName()));
        dest.setLabel(getLabelOrDefault(annotation.label(), orig.getName()));
        dest.setDescription(annotation.description());
        dest.setType(annotation.type());
        dest.setServiceUrl(annotation.serviceUrl());
        dest.setEditable(annotation.editable());
        dest.setType(getType(annotation.type(), orig));

        dest.setConstraints(propertyConstraintConverter.convert(annotation.constraints()));
        dest.setChoices(getChoices(annotation, (Class<?>) orig.getGenericType()));
    }

    private List<PropertyChoice> getChoices(TemplateProperty annotation, Class<?> genericType) {

        List<PropertyChoice> propertyChoices = propertyChoiceConverter.convertList(annotation.choices());

        if (propertyChoices.isEmpty() && genericType.isEnum()) {

            propertyChoices = Stream.of(genericType.getEnumConstants()).map(it -> (Enum<?>) it).map(it -> {
                PropertyChoice choice = new PropertyChoice();
                choice.setName(it.name());
                choice.setValue(it.name());
                return choice;
            }).collect(Collectors.toList());
        }

        return propertyChoices;
    }

    private String getLabelOrDefault(String groupLabel, String simpleName) {

        if (!StringUtils.equalsIgnoreCase(groupLabel, "#default")) {
            return groupLabel;
        }

        String[] array = StringUtils.splitByCharacterTypeCamelCase(simpleName);

        array[0] = StringUtils.capitalize(array[0]);

        return StringUtils.join(array, ' ');
    }


    private TemplatePropertyType getType(TemplatePropertyType type, Field orig) {

        if (type != TemplatePropertyType.UNDEFINED) {
            return type;
        }

        Class<?> genericType = (Class<?>) orig.getGenericType();

        return Optional.ofNullable(TYPE_MAPPING.get(genericType)).orElseThrow(() -> new IllegalArgumentException("Unable to find type of: " + genericType));
    }

    @SneakyThrows
    private Object getValue(Field orig, Object meta) {

        return FieldUtils.readField(orig, meta, true);
    }
}
