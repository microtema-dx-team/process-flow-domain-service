package de.microtema.process.domain.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.microtema.model.converter.Converter;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProcessInstanceStartEventToEventConverter implements Converter<String, ProcessInstanceStartEvent> {

    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public String convert(ProcessInstanceStartEvent orig) {

        return objectMapper.writeValueAsString(orig);
    }
}
