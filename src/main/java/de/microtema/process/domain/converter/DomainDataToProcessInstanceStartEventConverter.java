package de.microtema.process.domain.converter;

import de.microtema.model.converter.MetaConverter;
import de.microtema.process.domain.vo.CurrentUser;
import de.microtema.process.domain.vo.DomainData;
import de.microtema.process.domain.vo.ProcessInstanceStartEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class DomainDataToProcessInstanceStartEventConverter implements MetaConverter<ProcessInstanceStartEvent, DomainData, CurrentUser> {

    @Override
    public void update(ProcessInstanceStartEvent dest, DomainData orig, CurrentUser meta) {

        dest.setBusinessKey(UUID.randomUUID().toString());
        dest.setStartTime(LocalDateTime.now());

        dest.setReferenceId(orig.getReferenceId());
        dest.setReferenceType(orig.getReferenceType().name());
        dest.setReferenceValue(orig.getReferenceValue());

        dest.setDefinitionKey(orig.getDefinitionKey());
        dest.setProcessName(orig.getProcessName());
        dest.setBoundedContext(orig.getBoundedContext());

        dest.setStarterId(meta.getUsername());

        dest.setVariables(getVariables(dest));
    }

    private Map<String, Object> getVariables(ProcessInstanceStartEvent dest) {

        Map<String, Object> variables = new HashMap<>();

        variables.put("referenceId", dest.getReferenceId());
        variables.put("referenceType", dest.getReferenceType());
        variables.put("referenceValue", dest.getReferenceValue());
        variables.put("starterId", dest.getStarterId());

        return variables;
    }
}
