package de.microtema.process.domain.vo;

import de.microtema.process.domain.template.annotation.Constraint;
import de.microtema.process.domain.template.annotation.TemplateProperty;
import de.microtema.process.domain.template.enums.ReferenceType;
import de.microtema.process.domain.template.enums.TemplatePropertyType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DomainData {

    @NotNull
    @TemplateProperty(order = 0, constraints = @Constraint(notNull = true), description = "Reference Id is required and must be set")
    private String referenceId;

    @NotNull
    @TemplateProperty(order = 1, constraints = @Constraint(notNull = true), description = "Reference Type is required and must be set", type = TemplatePropertyType.DROPDOWN)
    private ReferenceType referenceType = ReferenceType.DOMAIN;

    @NotNull
    @TemplateProperty(order = 2, constraints = @Constraint(notNull = true), description = "Reference Value is required and must be set")
    private String referenceValue;

    @NotNull
    private String definitionKey;

    @NotNull
    private String processName;

    @NotNull
    private String boundedContext;
}
