package de.microtema.process.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

@Data
public class ProcessInstanceStartEvent {

    @NotEmpty
    private String starterId;

    @NotEmpty
    private String referenceId;

    @NotBlank
    private String referenceType;

    @NotNull
    private String referenceValue;

    @NotNull
    private String businessKey;

    @NotNull
    private Map<String, Object> variables = Collections.emptyMap();

    @NotNull
    private String definitionKey;

    @NotNull
    private String boundedContext;

    @NotNull
    private String processName;

    @NotNull
    private LocalDateTime startTime;
}
