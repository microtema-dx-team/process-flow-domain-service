package de.microtema.process.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CurrentUser {

    @NotEmpty
    public String username;
}
