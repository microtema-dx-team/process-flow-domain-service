package de.microtema.process.domain.vo;

public enum ResponseDataStatus {

    OK, ERROR
}
