package de.microtema.process.domain.vo;

import lombok.Value;

@Value
public class ResponseData<T> {

    T value;

    String message;

    ResponseDataStatus status;

    public static <T> ResponseData<T> ok(T value) {

        return new ResponseData<>(value, null, ResponseDataStatus.OK);
    }

    public static <T> ResponseData<T> error(String message) {

        return new ResponseData<>(null, message, ResponseDataStatus.ERROR);
    }
}
