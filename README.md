## Process-Flow domain-service

# create docker image

`docker build -t microtema/process-flow-domain-service:1.0.0 .`

# run docker file

`docker run --name process-flow-domain-service -p 8081:8080 microtema/process-flow-domain-service:1.0.0`

# push docker file

`docker push microtema/process-flow-domain-service:1.0.0`

# Endpoints

## Processes

* @POST /rest/process/start @Body DomainData @Response BusinessKey

## Template Properties

* @GET /rest/process/template @Response TemplateProperties

# Value Objects

## DomainData

| Property | Type | Required | Description |
|  --- | --- | --- | --- |
|  referenceId | String | True | Reference ID |
|  referenceType | String | True | Reference Type |
|  referenceValue | String | True | Reference Value |
|  businessKey | String | True | Custom generated Key |
|  boundedContext | String | True | bounded Context |
|  boundedContext | String | True | bounded Context |
|  majorVersion | Long | True | Major Version |
|  definitionVersion | Long | True | Definition Version |

## TemplateProperties

| Property | Type | Required | Description |
|  --- | --- | --- | --- |
|  name | String | True | Template Name |
|  id | String | True | Template Id |
|  properties | List < TemplateProperty > | True | List of @TemplateProperty |

## TemplateProperty

| Property | Type | Required | Description |
|  --- | --- | --- | --- |
|  value | Object | False | any value |
|  label | String | True | Property Label |
|  groupLabel | String | True | Property Group Label |
|  type | TemplatePropertyType | True | Property Type |
|  serviceUrl | String | False | Property Service Url |
|  editable | Boolean | True | Should Property be editable |
|  constraints | List < PropertyConstraint > | True | List of @PropertyConstraint |
|  choices | List < PropertyChoice > | True | List of PropertyChoice |

## PropertyChoice

| Property | Type | Required | Description |
|  --- | --- | --- | --- |
|  notNull | Boolean | False | Input must be non-empty |
|  minLength | Integer | False | Minimal length for the input |
|  maxLength | Integer | False | Maximal length for the input |
|  pattern | Integer | False | Regular expression to match the input against |

## PropertyConstraint

| Property | Type | Required | Description |
|  --- | --- | --- | --- |
|  name | String | True | Choice name |
|  name | Any | False | Choice value |

## TemplatePropertyType

| Element | Data Type | Element Type |
|  --- | --- | --- |
|  STRING | String | Input type:text |
|  BOOLEAN | Boolean | Checkbox |
|  DROPDOWN | List | Dropdown |
|  TEXT | String | Textarea |
|  HIDDEN | any | Input type:hidden |
|  NUMBER | Number | Input type:number |

# BusinessKey

BusinessKey is a composition of:

* ReferenceType
* ReferenceId
* ReferenceValue
* StarterId
* Timestamp

> NOTE: BusinessKey is still a weakness unique, but it is enough for this purpose,
> since we are not starting processes multiple times within 1 millisecond.